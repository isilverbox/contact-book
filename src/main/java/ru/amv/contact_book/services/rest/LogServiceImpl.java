package ru.amv.contact_book.services.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import ru.amv.contact_book.entity.LogEntity;
import ru.amv.contact_book.repository.LogRepository;
import ru.amv.contact_book.util.CaseConverter;

import javax.persistence.EntityManager;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class LogServiceImpl implements LogService {
	private static final Logger LOGGER = LoggerFactory.getLogger(LogServiceImpl.class);

	private final LogRepository logRepository;

	public LogServiceImpl(LogRepository logRepository) {
		this.logRepository = logRepository;
	}

	public List<LogEntity> getLogEntities(Map<String, String> map) {

		return logRepository.findAll((Specification<LogEntity>) (root, criteriaQuery, criteriaBuilder) -> {
			List<Predicate> predicates = new ArrayList<>();
			map.forEach((k, v) -> predicates.add(criteriaBuilder.equal((root.get(CaseConverter.snakeToCamel(k))), v)));
			criteriaQuery.where(predicates.toArray(Predicate[]::new));

			return null;
		});
	}
}