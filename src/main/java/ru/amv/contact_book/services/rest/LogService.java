package ru.amv.contact_book.services.rest;

import org.springframework.stereotype.Service;
import ru.amv.contact_book.entity.LogEntity;

import java.util.List;
import java.util.Map;

public interface LogService {
	List<LogEntity> getLogEntities(Map<String, String> map);
}
