package ru.amv.contact_book.services.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ru.amv.contact_book.dto.User;
import ru.amv.contact_book.entity.LogEntity;
import ru.amv.contact_book.repository.LogRepository;

@Service
@PropertySource("classpath:application.properties")
public class UserServiceImpl implements UserService{
	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	@Value("${rest.uri.user}")
	private String URI;

	private final ObjectMapper objectMapper = new ObjectMapper();
	private final RestTemplate restTemplate;
	private final LogRepository logRepository;

	public UserServiceImpl(RestTemplate restTemplate,
						   LogRepository logRepository) {
		this.restTemplate = restTemplate;
		this.logRepository = logRepository;
	}

	public String getUserName(MultiValueMap<String, String> params) {
		User user = getUser(params);
		return user == null ? null : user.getName();
	}

	public User getUser(MultiValueMap<String, String> params) {
		String uri = UriComponentsBuilder.fromHttpUrl(URI).queryParams(params).build().toUriString();

		ResponseEntity<String> responseEntity = restTemplate.getForEntity(uri, String.class);

		LogEntity logEntity = new LogEntity();
		logEntity.setClientRequest(params.toString());
		logEntity.setServiceRequest(uri);
		logEntity.setServiceResponseCode((short) responseEntity.getStatusCode().value());
		logEntity.setServiceResponse(responseEntity.getBody());

		LOGGER.info("Build URI: {}", logEntity.getServiceRequest());
		LOGGER.info("Response code: {}", logEntity.getServiceResponseCode());


		User[] users = null;
		try {
			users = objectMapper.readValue(responseEntity.getBody(), User[].class);
		} catch (JsonProcessingException | NullPointerException | ArrayIndexOutOfBoundsException e) {
			LOGGER.warn("Json to User error: {} parsing: {}", e.getMessage(), responseEntity.getBody());
		}

		User user = null;
		if (users != null && users.length > 0) {
			user = users[0];
			logEntity.setClientResponse(user.getName());
		}

		logRepository.save(logEntity);
		return user;
	}

}
