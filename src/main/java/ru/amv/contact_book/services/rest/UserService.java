package ru.amv.contact_book.services.rest;

import org.springframework.util.MultiValueMap;
import ru.amv.contact_book.dto.User;

public interface UserService {

	String getUserName(MultiValueMap<String, String> params);
	User getUser(MultiValueMap<String, String> params);
}
