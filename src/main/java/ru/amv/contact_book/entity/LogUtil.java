package ru.amv.contact_book.entity;

import java.util.List;

public class LogUtil {
	List<LogEntity> logEntities;

	public LogUtil(List<LogEntity> logEntities) {
		this.logEntities = logEntities;
	}

	public List<LogEntity> getLogEntities() {
		return logEntities;
	}

	public void setLogEntities(List<LogEntity> logEntities) {
		this.logEntities = logEntities;
	}
}
