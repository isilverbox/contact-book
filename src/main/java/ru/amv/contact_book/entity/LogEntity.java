package ru.amv.contact_book.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "log_entity")
public class LogEntity implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "service_response_code")
	private Short serviceResponseCode;

	@Column(name = "client_request")
	private String clientRequest;

	@Column(name = "client_response")
	private String clientResponse;

	@Column(name = "service_request")
	private String serviceRequest;

	@Column(name = "service_response", length = 5000, scale = 5000)
	private String serviceResponse;

	public LogEntity() {
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public Short getServiceResponseCode() {
		return serviceResponseCode;
	}

	public void setServiceResponseCode(Short serviceResponseCode) {
		this.serviceResponseCode = serviceResponseCode;
	}

	public String getClientRequest() {
		return clientRequest;
	}

	public void setClientRequest(String clientRequest) {
		this.clientRequest = clientRequest;
	}

	public String getClientResponse() {
		return clientResponse;
	}

	public void setClientResponse(String clientResponse) {
		this.clientResponse = clientResponse;
	}

	public String getServiceRequest() {
		return serviceRequest;
	}

	public void setServiceRequest(String serviceRequest) {
		this.serviceRequest = serviceRequest;
	}

	public String getServiceResponse() {
		return serviceResponse;
	}

	public void setServiceResponse(String serviceResponse) {
		this.serviceResponse = serviceResponse;
	}

	@Override
	public String toString() {
		return "LogEntity{" +
				"id=" + id +
				", serviceResponseCode=" + serviceResponseCode +
				", clientRequest='" + clientRequest + '\'' +
				", clientResponse='" + clientResponse + '\'' +
				", serviceRequest='" + serviceRequest + '\'' +
				", serviceResponse='" + serviceResponse + '\'' +
				'}';
	}
}
