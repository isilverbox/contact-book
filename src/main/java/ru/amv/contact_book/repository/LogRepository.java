package ru.amv.contact_book.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.amv.contact_book.entity.LogEntity;

import java.util.List;

public interface LogRepository extends CrudRepository<LogEntity, Integer>, JpaSpecificationExecutor<LogEntity>  {

	@Query(value = "SELECT * FROM LogEntity :cause", nativeQuery = true)
	List<LogEntity> find(String cause);


	LogEntity findById(long id);
//	List<LogEntity> findAll();

	List<LogEntity> findLogEntitiesByClientRequest(String clientRequest);
	List<LogEntity> findLogEntitiesByClientResponse(String clientResponse);
	List<LogEntity> findLogEntitiesByServiceRequest(String serviceRequest);
	List<LogEntity> findLogEntitiesByServiceResponse(String serviceResponse);
	List<LogEntity> findByServiceResponseCode(short serviceResponseCode);
}