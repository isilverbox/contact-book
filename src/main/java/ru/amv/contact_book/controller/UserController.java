package ru.amv.contact_book.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.amv.contact_book.services.rest.UserService;
import ru.amv.contact_book.services.rest.UserServiceImpl;

/**
 * Поиск осуществляется с параметрами столбец=ячейка, параметров может быть несколько, результат первое совпадение
 * пример http://localhost:8080/user/search?email=Shanna@melissa.tv&...
 */
@Controller()
@RequestMapping("/user")
public class UserController {
	private final static Logger LOGGER = LoggerFactory.getLogger(UserController.class);

	private final UserService userService;

	public UserController(UserService userService) {
		this.userService = userService;
	}

	@GetMapping
	public ModelAndView index(ModelAndView modelAndView) {
		modelAndView.setViewName("user/empty");

		return modelAndView;
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public String user(@RequestParam MultiValueMap<String, String> map,
					   Model model) {

		LOGGER.info("Trying to find user name by params: {}", map.toString());

		String name = userService.getUserName(map);

		model.addAttribute("name", name);
		return "user/user";
	}
}

