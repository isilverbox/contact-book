package ru.amv.contact_book.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.amv.contact_book.entity.LogEntity;
import ru.amv.contact_book.services.rest.LogService;
import ru.amv.contact_book.services.rest.LogServiceImpl;

import java.util.List;
import java.util.Map;

/**
 * Поиск осуществляется с параметрами столбец=ячейка, параметров как и результатов может быть несколько
 * Дополнительные параметры расширяют поиск
 * пример http://localhost:8080/log/select?id=6&client_response=Ervin Howell
 */
@Controller
@RequestMapping("/log")
public class LogController {
	private final static Logger LOGGER = LoggerFactory.getLogger(LogController.class);

	private final LogService logService;

	public LogController(LogService logService) {
		this.logService = logService;
	}

	@GetMapping
	public ModelAndView index (ModelAndView modelAndView) {
		modelAndView.setViewName("log/index");
		return modelAndView;
	}

	@RequestMapping(value = "/select", method = RequestMethod.GET)
	public String user(@RequestParam Map<String, String> map,
					   Model model) {

		LOGGER.info("Trying to find user name by params: {}", map.toString());

		List<LogEntity> logEntities = logService.getLogEntities(map);

		model.addAttribute("usersList", logEntities);

		return "log/log";
	}
}