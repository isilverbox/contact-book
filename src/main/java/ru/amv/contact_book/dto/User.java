package ru.amv.contact_book.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class User {
	private Long id;
	private String name;
	private String username;
	private String email;
	private String phone;
	private String website;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof User)) return false;
		User user = (User) o;
		return Objects.equals(getId(), user.getId()) &&
				Objects.equals(getName(), user.getName()) &&
				Objects.equals(getUsername(), user.getUsername()) &&
				Objects.equals(getEmail(), user.getEmail()) &&
				Objects.equals(getPhone(), user.getPhone()) &&
				Objects.equals(getWebsite(), user.getWebsite());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId(), getName(), getUsername(), getEmail(), getPhone(), getWebsite());
	}

	@Override
	public String toString() {
		return "UserDTO{" +
				"id=" + id +
				", name='" + name + '\'' +
				", username='" + username + '\'' +
				", email='" + email + '\'' +
				", phone='" + phone + '\'' +
				", website='" + website + '\'' +
				'}';
	}
}
