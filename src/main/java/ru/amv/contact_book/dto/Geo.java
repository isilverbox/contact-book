package ru.amv.contact_book.dto;

import java.util.Objects;

/**
 * Geo entity
 */
public class Geo {
	private double lat;
	private double lng;

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Geo)) return false;
		Geo geo = (Geo) o;
		return Double.compare(geo.getLat(), getLat()) == 0 &&
				Double.compare(geo.getLng(), getLng()) == 0;
	}

	@Override
	public int hashCode() {
		return Objects.hash(getLat(), getLng());
	}

	@Override
	public String toString() {
		return "Geo{" +
				"lat=" + lat +
				", lng=" + lng +
				'}';
	}
}
