package ru.amv.contact_book.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CaseConverter {
	private static final Logger LOGGER = LoggerFactory.getLogger(CaseConverter.class);

	public static String camelToSnake(String s) {
		return s != null ? s.replaceAll("([a-z])([A-Z]+)", "$1_$2").toLowerCase() : null;
	}

	public static String snakeToCamel(String s) {
		while (s.contains("_")) {
			s = s.replaceFirst("_[a-z]", String.valueOf(Character.toUpperCase(s.charAt(s.indexOf("_") + 1))));
		}
		return s;
	}
}