package ru.amv.contact_book.configuration;

import org.apache.http.client.config.CookieSpecs;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.apache.http.client.config.RequestConfig;


@Configuration
public class RestConfig {

	@Bean
	public RestTemplate restTemplate(
			@Value("${rest.request.timeout:30000}") int connectionRequestTimeout,
			@Value("${rest.connection.timeout:30000}") int connectTimeout,
			@Value("${rest.read.timeout:30000}") int readTimeout,
			@Value("${rest.max.connections:10}") int maxConnectionsCount,
			@Value("${proxy.list:#{null}}") String proxyList) {
		return createRestTemplate(connectionRequestTimeout, connectTimeout, readTimeout, maxConnectionsCount);
	}

	private RestTemplate createRestTemplate(int connectionRequestTimeout,
											int connectTimeout,
											int readTimeout,
											int maxConnectionsCount) {

		RequestConfig requestConfig = RequestConfig.custom().setCookieSpec(CookieSpecs.IGNORE_COOKIES).build();
		HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
		if (maxConnectionsCount > 2) {
			PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
			connectionManager.setMaxTotal(maxConnectionsCount);
			connectionManager.setDefaultMaxPerRoute(maxConnectionsCount / 2);
			httpClientBuilder.setConnectionManager(connectionManager);
			httpClientBuilder.setDefaultRequestConfig(requestConfig).disableCookieManagement().disableRedirectHandling();
		}
		CloseableHttpClient httpClient = httpClientBuilder.disableCookieManagement().build();

		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		requestFactory.setHttpClient(httpClient);
		requestFactory.setConnectionRequestTimeout(connectionRequestTimeout);
		requestFactory.setConnectTimeout(connectTimeout);
		requestFactory.setReadTimeout(readTimeout);
		return new RestTemplate(requestFactory);
	}

}