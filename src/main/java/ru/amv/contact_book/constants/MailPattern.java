package ru.amv.contact_book.constants;

public class MailPattern {
	public static final String EMAIL = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
}
